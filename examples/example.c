#include "server.h"

char* exampleGetPage(const char* url, struct connectionInfo* conInfo, size_t* size) {
  static const char* template =
    "<!DOCTYPE html><html><body>"\
    "<h1>%s</h1>"\
    "<form action=\"/\" method=\"post\">"\
    "<input type=\"text\" name=\"foo\"><br>"\
    "<input type=\"text\" name=\"bar\"><br>"\
    "<input type=\"submit\" value=\"submit\">"\
    "</form>"\
    "</body></html>";
  size_t templateSzSansPlaceholders = strlen(template)-2;
  *size = templateSzSansPlaceholders + strlen(url) + 1;
  char* page = malloc(*size);
  snprintf(page, *size, template, url);
  *size -= 1; //We don't want to return a nul to a browser
  return page;
}

char* examplePostPage(const char* url, struct connectionInfo* conInfo, size_t* size) {
  static const char* nullstr = "";
  static const char* template =
    "<!DOCTYPE html><html><body>"\
    "<h1>%s</h1>"\
    "<p>foo: %s"\
    "<p>bar: %s"\
    "</body></html>";
  const struct postParam *fooParam = ppGet(conInfo, "foo"), *barParam = ppGet(conInfo, "bar");
  const char *foo = fooParam?fooParam->data:nullstr;
  const char *bar = barParam?barParam->data:nullstr;
  size_t templateSzSansPlaceholders = strlen(template)-6;
  *size = templateSzSansPlaceholders + strlen(url) + strlen(foo) + strlen(bar) + 1;
  char* page = malloc(*size);
  snprintf(page, *size, template, url, foo, bar);
  *size -= 1; //We don't want to return a nul to a browser
  return page;
}

int main (void) {
  return startServer(8080, &exampleGetPage, &examplePostPage);
}
