FLAGS=-g -Wall -Wpedantic -Wextra -Wno-unused-parameter
LINK=-lmicrohttpd

all: server.c server.h
	@gcc -c $(FLAGS) server.c -o server.o $(LINK)
	@ar rcs libserver.a server.o
	@grep 'TODO' server.c server.h || true
	@rm server.o

install: libserver.a
	@sudo cp libserver.a /usr/local/lib/libserver.a
	@sudo cp server.h /usr/local/include/server.h

clean: libserver.a
	@rm libserver.a
