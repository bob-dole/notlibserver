#include "server.h"
#include <time.h>
#define POST_BUFFER_SIZE 65536
static pageMakingFunctionPtr getPage = NULL, postPage = NULL;
static size_t nextConnectionID = 0;
static int serverStopping = 0;

char* strcpy_alloc (const char* src) {
  size_t sz = strlen(src)+1;
  char* str = (char*) malloc(sz);
  if (!str)
    exit(1);
  strncpy(str, src, sz);
  return str;
}

static struct connectionInfo* createConnectionInfo(enum connectionType type, const char* sessionKey, int newSession) {
  struct connectionInfo* conInfo = malloc(sizeof(struct connectionInfo));
  if (conInfo) {
    conInfo->connectionID = nextConnectionID++;
    conInfo->type = type;
    conInfo->postParameterCapacity = 10;
    conInfo->postParameterCount = 0;
    size_t paramArraySz = sizeof(struct postParam*)*conInfo->postParameterCapacity;
    conInfo->postParameters = malloc(paramArraySz);
    memset(conInfo->postParameters, 0, paramArraySz);
    conInfo->postProcessor = NULL;
    conInfo->sessionKey = strcpy_alloc(sessionKey);
    conInfo->newSession = newSession;
  }
  return conInfo;
}

static void postParamFree(struct postParam* pp) {
  free(pp->key);
  free(pp->data);
  free(pp);
}

static void ppFree(struct connectionInfo* conInfo) {
  for (size_t i = 0; i < conInfo->postParameterCount; ++i)
    if (conInfo->postParameters[i])
      postParamFree(conInfo->postParameters[i]);
  free(conInfo->postParameters);
  conInfo->postParameters = NULL;
}

static void freeConnectionInfo(struct connectionInfo* conInfo) {
  if (!conInfo)
    return;
  if (conInfo->type == POST) {
    MHD_destroy_post_processor(conInfo->postProcessor);
  }
  free(conInfo->sessionKey);
  ppFree(conInfo);
  free(conInfo);
}

static void ppAdd(struct connectionInfo* conInfo, const char* key, const char* data, size_t dataSz) {
  if (conInfo->postParameterCount < conInfo->postParameterCapacity) {
    conInfo->postParameters[conInfo->postParameterCount] = malloc(sizeof(struct postParam));
    ++dataSz; //We add a trailing NUL to post data (NB: if changed ensure zero-length post data works correctly since we'll be malloc(0)ing.)
    struct postParam* pp = conInfo->postParameters[conInfo->postParameterCount];
    if (!pp)
      exit(1);
    pp->key = strcpy_alloc(key);
    pp->data = (char*)malloc(dataSz);
    pp->dataSz = dataSz;
    if (!pp->data)
      exit(1);
    memcpy(pp->data, data, dataSz-1);
    pp->data[dataSz-1] = '\0';
    
    ++conInfo->postParameterCount;
  } else {
    conInfo->postParameterCapacity *= 2;
    conInfo->postParameters = realloc(conInfo->postParameters, sizeof(struct postParam)*(conInfo->postParameterCapacity));
    if (!conInfo->postParameters)
      exit(1);
    ppAdd(conInfo, key, data, dataSz);
  }
}

int ppContains(struct connectionInfo* conInfo, const char* key) {
  return NULL != ppGet(conInfo, key);
}

//Returns NULL on failure
const struct postParam* ppGet(struct connectionInfo* conInfo, const char* key) {
  for (size_t i = 0; i < conInfo->postParameterCount; ++i) {
    struct postParam* pp = conInfo->postParameters[i];
    if (!strcmp(key, pp->key))
      return pp;
  }
  return NULL;
}

static int examineKeyvalue(void* cls, enum MHD_ValueKind kind, const char* key, const char* value) {
  return MHD_YES;
}

static int iteratePost(void* coninfo_cls, enum MHD_ValueKind kind, const char* key, const char* filename, const char* content_type, const char* transfer_encoding, const char* data, uint64_t off, size_t size) {
  struct connectionInfo* conInfo = coninfo_cls;
  if (!ppContains(conInfo, key)) {
    ppAdd(conInfo, key, data, size);
  } else {
    //printf("%zu: Ignoring duplicate post parameter key: \"%s\" with data \"%s\" (%zu bytes).\n", conInfo->connectionID, key, data?data:"", size);
  }
  
  return MHD_YES;
}

int sendResponse(struct MHD_Connection* connection, const char* page, size_t pageSz, int newSession, const char* sessionKey) {
  if (page) {
    struct MHD_Response* response = MHD_create_response_from_buffer(pageSz, (void*)page, MHD_RESPMEM_MUST_FREE);
    MHD_add_response_header(response, "Access-Control-Allow-Origin", "*");
    
    //sessionKey cookie
    const char* prefix = "sessionKey=";
    const char* suffix = "; Path=/";
    int sz = strlen(prefix)+strlen(sessionKey)+strlen(suffix)+1;
    char* headerValue = NULL; //sessionKey=$sessionKey
    if (newSession) {
      headerValue = malloc(sz);
      snprintf(headerValue, sz, "%s%s%s", prefix, sessionKey, suffix);
      MHD_add_response_header(response, "Set-Cookie", headerValue);
    }
    
    int ret = MHD_queue_response(connection, MHD_HTTP_OK, response);
    MHD_destroy_response(response); //internal ref counting ensures it isn't freed before it's sent
    if (headerValue)
      free(headerValue);
    return ret;
  } else {
    printf("ERROR: Internal NULL pointer.\n");
    exit(1);
  }
}

static int initConnection(void* cls,
			  struct MHD_Connection* connection,
			  const char* url,
			  const char* method,
			  const char* version,
			  const char* uploadData,
			  size_t* uploadDataSz,
			  void** con_cls) {
  struct connectionInfo* conInfo = NULL;
  
  //Look for session cookie
  const char* sessionKey = MHD_lookup_connection_value(connection, MHD_COOKIE_KIND, "sessionKey");
  int newSession = 0;
  size_t keysize = 256;
  char key[keysize+1];
  if (!sessionKey) {
    for (size_t i = 0; i < keysize; ++i) {
      key[i] = ((rand()%2)?'a':'A')+(rand()%26);
    }
    key[keysize] = '\0';
    sessionKey = key;
    newSession = 1;
  }
  
  if (!strcmp("GET", method)) {
    conInfo = createConnectionInfo(GET, sessionKey, newSession);
  } else if (!strcmp("POST", method)) {
    conInfo = createConnectionInfo(POST, sessionKey, newSession);
    conInfo->postProcessor = MHD_create_post_processor(connection, POST_BUFFER_SIZE, iteratePost, (void*) conInfo);
    if (!conInfo->postProcessor) {
      freeConnectionInfo(conInfo);
      conInfo = NULL;
    }
  } else {
    printf("%zu: Unknown method: \"%s\"\n", conInfo->connectionID, method);
  }
  if (!conInfo)
    return MHD_NO;
  *con_cls = (void*)conInfo;
  return MHD_YES;
}

static int continueConnection (void* cls,
			       struct MHD_Connection* connection,
			       const char* url,
			       const char* method,
			       const char* version,
			       const char* uploadData,
			       size_t* uploadDataSz,
			       void** con_cls) {
  struct connectionInfo* conInfo = *con_cls;
  char* page = NULL;
  size_t pageSz = 0;
  
  //Examine key/value pairs
  MHD_get_connection_values(connection, MHD_HEADER_KIND | MHD_COOKIE_KIND | MHD_POSTDATA_KIND | MHD_GET_ARGUMENT_KIND | MHD_FOOTER_KIND, examineKeyvalue, NULL);
  
  if (!strcmp("GET", method)) {
    page = getPage(url, conInfo, &pageSz);
  } else if (!strcmp("POST", method)) {
    if (*uploadDataSz) {
      //Middling call
      MHD_post_process(conInfo->postProcessor, uploadData, *uploadDataSz);
      *uploadDataSz = 0;
      return MHD_YES;
    } else {
      //Final call
      //Check for a POSTed sessionKey (XHRs don't send cookies)
      if (ppContains(conInfo, "sessionKey")) {
        conInfo->newSession = 0;
        free(conInfo->sessionKey);
        conInfo->sessionKey = strcpy_alloc(ppGet(conInfo, "sessionKey")->data);
      }
      page = postPage(url, conInfo, &pageSz);
    }
  } else {
    printf("%zu: Unknown method: \"%s\"\n", conInfo->connectionID, method);
    return MHD_NO;
  }
  
  return sendResponse(connection, page, pageSz, conInfo->newSession, conInfo->sessionKey);
}

//Sets *con_cls to point to an int which is equal to the number of times connectionCLBK has been called for this connection before now.
static int connectionCLBK (void* cls,
			   struct MHD_Connection* connection,
			   const char* url,
			   const char* method,
			   const char* version,
			   const char* uploadData,
			   size_t* uploadDataSz,
			   void** con_cls) {
  struct connectionInfo* conInfo = *con_cls;
  if (!((!strcmp(method, "GET")) || (!strcmp(method, "POST")))) {
    printf("%zu: Method rejected: \"%s\"\n", conInfo->connectionID, method);
    return MHD_NO;
  }
  
  if (*con_cls) {
    return continueConnection(cls, connection, url, method, version, uploadData, uploadDataSz, con_cls);
  } else {
    return initConnection(cls, connection, url, method, version, uploadData, uploadDataSz, con_cls);
  }
}

static void requestCompleted(void* cls, struct MHD_Connection* connection, void** con_cls, enum MHD_RequestTerminationCode requestTerminationCode) {
  struct connectionInfo* conInfo = (struct connectionInfo*)*con_cls;
  //printf("%zu: Request completed.\n", conInfo->connectionID);
  freeConnectionInfo(conInfo);
  *con_cls = NULL;
}

static int onNewConnection(void* cls, const struct sockaddr* addr, socklen_t addrlen) {
  //WARNING: inet_ntoa isn't thread safe as subsequent calls overwrite the return value
  //u_short port = ((struct sockaddr_in*)addr)->sin_port;
  //printf("Request from %s:%d\n", inet_ntoa(((struct sockaddr_in*)addr)->sin_addr), port);
  return MHD_YES;
}

int startServer(short port, pageMakingFunctionPtr GetPage, pageMakingFunctionPtr PostPage) {
  //Start server
  srand(time(NULL));
  serverStopping = 0;
  getPage = GetPage;
  postPage = PostPage;
  //unsigned int flags = MHD_USE_AUTO | MHD_USE_INTERNAL_POLLING_THREAD | MHD_USE_PEDANTIC_CHECKS //for newer version of the library
  unsigned int flags = MHD_USE_POLL | MHD_USE_SELECT_INTERNALLY | MHD_USE_THREAD_PER_CONNECTION;
  struct MHD_Daemon* daemon = MHD_start_daemon(flags,
					       port,
					       onNewConnection, NULL,
					       &connectionCLBK, NULL,
					       MHD_OPTION_NOTIFY_COMPLETED, &requestCompleted, NULL,
					       MHD_OPTION_END);
  if (!daemon)
    return 1;
  
  //Terminate server when appropriate
  printf("Serving on 127.0.0.1:%d, press any key to terminate server.\n", port);
  while (!serverStopping) {
    sleep(5);
  }
  MHD_stop_daemon(daemon);
  return 0;
}

void stopServer() {
  serverStopping = 1;
}
