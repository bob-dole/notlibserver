#ifndef SERVER_H
#define SERVER_H
#include <sys/types.h>
#include <sys/select.h>
#include <sys/socket.h>
#include <microhttpd.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#ifdef __cplusplus
extern "C" {
#endif
  enum connectionType {POST, GET};
  
  struct postParam {
    char *key, *data; //WARNING: data has a tailing NUL appended, this is reflected in dataSz.
    size_t dataSz;
  };
  
  struct connectionInfo {
    size_t connectionID;
    enum connectionType type;
    struct postParam** postParameters;
    size_t postParameterCount;
    size_t postParameterCapacity;
    struct MHD_PostProcessor* postProcessor;
    char* sessionKey;
    int newSession; //True if we need to set the sessionKey cookie for a new session
  };
  
  //Ought to return an newly allocated string to be free()d by the server when it's done with it.
  typedef char * (*pageMakingFunctionPtr)(const char *, struct connectionInfo *, size_t* dataSz);
  typedef char * (pageMakingFunction)(const char *, struct connectionInfo *, size_t* dataSz);
  
  //Returns 0 on success, nonzero on failure.
  int startServer(short port, pageMakingFunctionPtr GetPage, pageMakingFunctionPtr PostPage);
  void stopServer();
  
  //Post parameters
  int ppContains(struct connectionInfo* conInfo, const char* key);
  const struct postParam* ppGet(struct connectionInfo* conInfo, const char* key);
#ifdef __cplusplus
}
#endif
#endif
